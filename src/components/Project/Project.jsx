import React from "react";
import "./Project.css";
import { useEffect, useState } from "react";
import CardProject from "../CardProject/CardProject";
import data from "../../data/projectList";

function Project() {
  const [projects, setProjects] = useState([]);
  useEffect(() => {
    setProjects(data);
  }, []);

  return (
    <section id="projects">
      <h1>Mes projets</h1>
      <div className="d-flex flex-row flex-wrap gap-5 my-3">
        {projects.map((project) => (
          <CardProject
            key={project.id}
            id={project.id}
            title={project.title}
            date={project.date}
            category={project.category}
            logo={project.logo}
            miniature={project.miniature}
          />
        ))}
      </div>
    </section>
  );
}

export default Project;
