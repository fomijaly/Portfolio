import React from 'react';
import './SkillsResponsive.css';
import ButtonSkills from '../../Buttons/ButtonSkills';
import skills from '../../../assets/skills'

function SkillsResponsive() {
  return (
      <div className='all-skills-responsive flex-wrap gap-2'>
        {
          skills.map(skill => {
            return(
              <ButtonSkills
                key={skill.id}
                title={skill.title}
                class={skill.class}
              />
            )
          })
        }
      </div>
  )
}

export default SkillsResponsive;