import React from "react";
import "./Competence.css";
import SkillsWeb from "./SkillsWeb/SkillsWeb";
import SkillsResponsive from "./SkillsResponsive/SkillsResponsive";

function Competence() {
  return (
    <section id="skills">
      <h1>Mes skills</h1>
      <div className="d-flex flex-column gap-4">
        <article className="mt-3">
          Dans le cadre de mon expérience professionnelle en tant que webmaster,
          de ma formation autodidacte, des formations professionnelles ou encore
          de ma vie quotidienne, j'ai développé les savoirs-être et les
          savoirs-faire ci-dessous.
        </article>
        <SkillsResponsive/>
        <SkillsWeb/> 
      </div>
    </section>
  );
}

export default Competence;
