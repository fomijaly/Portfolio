import React from "react";
import "./SkillsWeb.css";
import skills from "../../../assets/skills";
import ButtonSkillsWeb from "../../Buttons/ButtonSkillsWeb";
import ButtonSoftSkillsWeb from "../../Buttons/ButtonSoftSkillsWeb";

function SkillsWeb() {
  return (
    <>
      <div className="all-skills justify-content-between my-3">
        <div className="d-flex flex-column">
          <h2>Design</h2>
          {skills.map((skill) => {
            if (skill.tag == "Design") {
              return (
                <ButtonSkillsWeb
                  key={skill.id}
                  title={skill.title}
                  picture={skill.picture}
                />
              );
            }
          })}
        </div>
        <div className="d-flex flex-column">
          <h2>Frontend</h2>
          {skills.map((skill) => {
            if (skill.tag == "Front") {
              return (
                <ButtonSkillsWeb
                  key={skill.id}
                  title={skill.title}
                  picture={skill.picture}
                />
              );
            }
          })}
        </div>
        <div className="d-flex flex-column">
          <h2>Backend</h2>
          {skills.map((skill) => {
            if (skill.tag == "Back") {
              return (
                <ButtonSkillsWeb
                  key={skill.id}
                  title={skill.title}
                  picture={skill.picture}
                />
              );
            }
          })}
        </div>
        <div className="d-flex flex-column">
          <h2>Other</h2>
          {skills.map((skill) => {
            if (skill.tag == "Other") {
              return (
                <ButtonSkillsWeb
                  key={skill.id}
                  title={skill.title}
                  picture={skill.picture}
                />
              );
            }
          })}
        </div>
      </div>
      <div className="all-skills">
        <div className="d-flex flex-column">
          <h2>Soft skills</h2>
          <div className="d-flex flex-wrap gap-3">
            {skills.map((skill) => {
              if (skill.tag == "Soft") {
                return (
                    <ButtonSoftSkillsWeb
                      key={skill.id} 
                      title={skill.title}
                      picture={skill.picture}
                      class={skill.class}
                    />
                );
              }
            })}
          </div>
        </div>
      </div>
    </>
  );
}

export default SkillsWeb;
