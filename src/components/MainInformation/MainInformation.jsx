import React from 'react';
import './MainInformation.css';
import Header from './Header/Header';
import ImagePortfolio from '../../assets/photo-portfolio.png'
import Information from './Information/Information';
import ButtonsNavigation from "../ButtonsNavigation/ButtonsNavigation.jsx";

function MainInformation() {
    return (
        <>
            <section id='about'>
                <Header></Header>
                <Information></Information>
                {/*<img src={ImagePortfolio} className='photo'></img>*/}
            </section>
            <ButtonsNavigation></ButtonsNavigation>
        </>
    )
}

export default MainInformation;