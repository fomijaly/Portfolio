import React from "react";
import "./Header.css";
import { NavLink } from "react-router-dom";
import { HashLink as Link } from "react-router-hash-link";
import PhotoContact from "../../../assets/contact-photo.png";

function Header() {
  return (
    <>
      <div className="version-web">
        <div className="version-web d-flex justify-content-between pb-3">
          <div className="d-flex flex-row gap-3">
            <NavLink
              to={"mailto:marinethomas.pro@outlook.fr"}
              className={"d-flex  align-items-center gap-2"}
            >
              <i className="bx bxs-paper-plane"></i>
              marinethomas.pro@outlook.fr
            </NavLink>
            <button className="btn px-4 fw-medium btn-rounded">
              <NavLink
                to={
                  "http://portfolio.marinethomas.ovh/contenu/CV%20-%20Marine%20Thomas%20-%20Alternance%20Conceptrice%20Développeuse%20d'Applications.pdf"
                }
              >
                CV
              </NavLink>
            </button>
          </div>
          <NavLink to={"https://www.linkedin.com/in/marine-thomas-cda/"}>
            LinkedIn
          </NavLink>
        </div>
        <hr />
      </div>
      <nav className="version-responsive navbar navbar-dark">
        <div className="container-fluid">
          <button
            className="navbar-toggler box"
            type="button"
            data-bs-toggle="offcanvas"
            data-bs-target="#offcanvasDarkNavbar"
            aria-controls="offcanvasDarkNavbar"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
          <div
            className="offcanvas offcanvas-end text-bg-dark"
            tabIndex="-1"
            id="offcanvasDarkNavbar"
          >
            <div className="offcanvas-header">
              <button
                type="button"
                className="btn-close btn-close-white"
                data-bs-dismiss="offcanvas"
              ></button>
            </div>
            <div className="offcanvas-body">
              <ul className="custom-nav navbar-nav justify-content-end flex-grow-1 px-5 gap-4">
                <li data-bs-dismiss="offcanvas">
                  <Link
                    to={"/#presentation"}
                    className="d-flex align-items-center gap-2 "
                  >
                    A propos
                  </Link>
                </li>
                <li data-bs-dismiss="offcanvas">
                  <Link
                    to={"/#timeline"}
                    className="d-flex align-items-center gap-2 "
                  >
                    Timeline
                  </Link>
                </li>
                <li data-bs-dismiss="offcanvas">
                  <Link
                    to={"/#skills"}
                    className="d-flex align-items-center gap-2 "
                  >
                    Skills
                  </Link>
                </li>
                <li data-bs-dismiss="offcanvas">
                  <Link
                    to={"/#projects"}
                    className="d-flex align-items-center gap-2 "
                  >
                    Projets
                  </Link>
                </li>
              </ul>
              <div className="social-medias d-flex flex-column position-absolute bottom-0 start-0 mb-4 px-4">
                <hr />
                <div className="d-flex justify-content-between align-items-end px-5">
                  <NavLink
                    to={"https://www.linkedin.com/in/marine-thomas-cda"}
                    className={
                      "d-flex flex-column justify-content-center align-items-center gap-2"
                    }
                  >
                    <i className="bx bxl-linkedin bx-sm"></i>
                  </NavLink>
                  <NavLink
                    to={"mailto:marinethomas.pro@outlook.fr"}
                    className={
                      "position-relative d-flex flex-column justify-content-center align-items-center gap-2"
                    }
                  >
                    <img
                      src={PhotoContact}
                      alt="Me contacter"
                      className="photo-contact"
                    />
                    <i className="contact-icon bx bxs-envelope bx-xs position-absolute rounded-circle"></i>
                  </NavLink>
                  <NavLink
                    to={
                      "http://portfolio.marinethomas.ovh/contenu/CV%20-%20Marine%20Thomas%20-%20Alternance%20Conceptrice%20Développeuse%20d'Applications.pdf"
                    }
                    className={"d-flex align-items-center fw-bold"}
                  >
                    CV
                  </NavLink>
                </div>
              </div>
            </div>
          </div>
        </div>
      </nav>
    </>
  );
}

export default Header;
