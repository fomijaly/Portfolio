import React from "react";
import "./Information.css";
import LogoSimplon from '../../../assets/logo-simplon.png';
import LogoOP from '../../../assets/logo-onepoint.png';
import { NavLink } from "react-router-dom";

function Information() {
  return (
    <div className=" d-flex align-items-center details">
      <div className="d-flex justify-content-center flex-column details-content">
        <div className="d-flex flex-column gap-4">
          <h1>Conceptrice <br/> Développeuse d'Applications</h1>
          <p>En formation avec
            <NavLink to={"https://occitanie.simplon.co"} target="_blank"  className="d-inline-flex align-items-center word-bg px-1 mx-1 fw-medium gap-1">
              <img src={LogoSimplon} alt="Logo Simplon"/>
              Simplon
            </NavLink>, je poursuis mon alternance au sein de <NavLink to={"https://www.groupeonepoint.com/fr/"} target="_blank" className="d-inline-flex align-items-center word-bg px-1 mx-1 fw-medium gap-1">
              <img src={LogoOP} alt="Logo Simplon"/>
              OnePoint
            </NavLink>, grand architecte de la transformation numérique, afin d’obtenir mon titre professionnel. </p>
          <div className="d-flex flex-row gap-3">
            <button className="d-flex align-items-center gap-2 btn btn-rounded btn-github pr-4">
              <i className="bx bxl-github"></i>
              Github
            </button>
            <button className="d-flex align-items-center gap-2 btn btn-rounded btn-light btn-gitlab pr-4">
              <i className="bx bxl-gitlab"></i>
              Gitlab
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Information;
