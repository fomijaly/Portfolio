import React from "react";
import "./CardProject.css";
import { Link } from "react-router-dom";

function CardProject({ id, title, date, category, logo, miniature }) {
  return (
    <div className="card_project my-4">
        <Link to={`/detail/${id}`} style={{color: 'var(--main-black)'}}>
            <img className="miniature rounded" src={miniature} alt="titre image" />
            <div className="d-flex flex-row justify-content-between align-items-center py-2">
                <div className="d-flex flex-row rounded align-items-center gap-2">
                    <img className="logo_card_project" src={logo}/>
                    <span className="title">{title}</span>
                    <span className="card_tag px-1 rounded-3 text-dark">{category}</span>
                </div>
                <span><i className='bx bxs-calendar'></i>{date}</span>
            </div>
        </Link>
    </div>
  );
}

export default CardProject;
