import React from "react";
import { NavLink } from "react-router-dom";
import "./AboutMe.css";

function AboutMe() {
  return (
    <section
      id="presentation"
      className="d-flex flex-column justify-content-center align-items-center gap-3"
    >
      <h1>Marine THOMAS 👋🏽</h1>

      <article className="presentation-bloc d-flex flex-column justify-content-center">
        <p>
          🌈 Passionnée par la programmation je souhaite mettre mes compétences
          afin d'
          <b className="word-bold">
            améliorer la productivité des entreprises
          </b>{" "}
          en créant de nouveaux outils ou en améliorant et maintenant
          l'existant.
        </p>
        <p>
          👩🏽‍🎓 J'ai intégré en <b className="word-bold">octobre 2023</b>,
          l'organisme de formation Simplon, afin d'obtenir mon
          <NavLink
            to={"https://www.francecompetences.fr/recherche/rncp/31678/"}
            target="_blank"
            className="d-inline-flex align-items-center word-bg px-1 mx-1 fw-medium gap-1"
          >
            <i className="bx bx-link-external bx-tada"></i>
            titre Professionnel
          </NavLink>
          de Conceptrice développeuse d'applications.
        </p>
        <p>
          🔵 Pour pratiquer et mettre à profit les compétences acquises pendant
          cette formation, j'ai intégré l'entreprise Onepoint, en phase avec
          mes valeurs.
        </p>
      </article>

      <div className="about d-flex justify-content-around text-center mt-5">
        <div className="about-bloc with-border-right rounded">
          <h1>15</h1>
          Projets
        </div>
        <div className="about-bloc with-border-right rounded">
          <h1>8</h1>
          Certifications
        </div>
        <div className="about-bloc rounded">
          <h1>2</h1>
          Expériences
          <br />
          professionnelles
        </div>
      </div>
    </section>
  );
}

export default AboutMe;
