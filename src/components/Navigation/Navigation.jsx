import React from 'react';
import { HashLink as Link } from 'react-router-hash-link';
import './Navigation.css';

function Navigation() {
  return (
    <>
      <nav className='main-nav align-items-center flex-row-reverse sticky-top'>
        <ol className='d-flex gap-5 py-3'>
          <li><Link to={'/#presentation'}>A propos</Link></li>
          <li><Link to={'/#skills'}>Skills</Link></li>
          <li><Link to={'/#projects'}>Projets</Link></li>
          <li><Link to={'/timeline'}>Timeline</Link></li>
        </ol>
      </nav>
    </>
  )
}

export default Navigation;