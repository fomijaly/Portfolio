import React from "react";
import "./Timeline.css";
import WorkIcon from "../../assets/WorkIcon";
import PersonelIcon from "../../assets/PersonelIcon";
import SchoolIcon from "../../assets/SchoolIcon";
import timelineElements from "../../assets/timeline";
import {
  VerticalTimeline,
  VerticalTimelineElement,
} from "react-vertical-timeline-component";
import "react-vertical-timeline-component/style.min.css";

function Timeline() {
  let workIconStyles = { background: "var(--main-green)" };
  let schoolIconStyles = { background: "#fff" };
  let personalIconStyles = { background: "var(--light-green)", color : 'red' };

  return (
    <>
      <VerticalTimeline 
        layout={"2-columns"} 
        lineColor={'#fff'}
      >
        {timelineElements.map((element) => {
          let isWorkIcon = element.tag === "Professionnel";
          let isPersonalIcon = element.tag === "Personnel";
          let iconStyle;
          let icon;
          if (isWorkIcon) {
            iconStyle = workIconStyles;
            icon = <WorkIcon width="24" height="24" />;
          } else if (isPersonalIcon) {
            iconStyle = personalIconStyles;
            icon = <PersonelIcon width="24" height="24" />;
          } else {
            iconStyle = schoolIconStyles;
            icon = <SchoolIcon width="24" height="24" />;
          }
          return (
            <VerticalTimelineElement
              key={element.id}
              tag={element.tag}
              icon={icon}
              iconStyle={iconStyle}
              location={element.location}
              dateClassName="date"
              date={element.last_date !== null ? element.first_date + " > " + element.last_date : element.first_date}
              contentStyle={ {boxShadow: "none"} }
            >
              <h3 className="vertical-timeline-element-title">
                {element.title}
              </h3>
              <h5 className="vertical-timeline-element-subtitle">
                {element.location}
              </h5>
              <p id="description">{element.description}</p>
            </VerticalTimelineElement>
          );
        })}
      </VerticalTimeline>
    </>
  );
}

export default Timeline;
