import React from 'react';
import './ButtonSkills.css'

function ButtonSkills(buttonDetails) {
  return (
    <span className={buttonDetails.class}>
      {buttonDetails.title}
    </span>
  )
}

export default ButtonSkills