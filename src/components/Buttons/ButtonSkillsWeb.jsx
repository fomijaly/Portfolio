import React from "react";
import "./ButtonSkills.css";

function ButtonSkillsWeb(buttonDetails) {
  return (
    <span className={buttonDetails.class}>
      <img
        className="icon-skills"
        src={buttonDetails.picture}
        alt={buttonDetails.title}
      />
      {buttonDetails.title}
    </span>
  );
}

export default ButtonSkillsWeb;
