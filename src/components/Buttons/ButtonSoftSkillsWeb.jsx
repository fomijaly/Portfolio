import React from "react";
import "./ButtonSkills.css";
import "../Buttons/ButtonSkills.css";

function ButtonSoftSkillsWeb(buttonDetails) {
  return (
    <span className={buttonDetails.class}>
      <span>{buttonDetails.picture}</span>
      {buttonDetails.title}
    </span>
  );
}

export default ButtonSoftSkillsWeb;
