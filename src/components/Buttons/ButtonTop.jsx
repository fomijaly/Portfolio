import React from 'react';
import './ButtonTop.css';
import { HashLink as Link } from 'react-router-hash-link';

function ButtonTop() {
  return (
    <button className='buttonTop d-flex flex-column align-items-center'>
      <i className='fa fa-arrow-up'></i>
      <Link to={'/#about'}>Top</Link>
    </button>
  )
}

export default ButtonTop;