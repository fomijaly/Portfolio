import React from 'react';
import './ButtonsNavigation.css';
import {HashLink as Link} from "react-router-hash-link";

function ButtonsNavigation() {
  return (
    <div className="box_card d-flex justify-content-center align-items-center">
        <div className="card d-flex flex-row align-items-center">
        <button>
            <Link to={'/'}>Accueil</Link>
        </button>
        <div className="card d-flex flex-row">
            <button>
                <Link to={'/#presentation'}>A propos</Link>
            </button>
            <button>
                <Link to={'/#skills'}>Skills</Link>
            </button>
            <button>
                <Link to={'/#projects'}>Projets</Link>
            </button>
            <button>
                <Link to={'/timeline'}>Mon parcours</Link>
            </button>
        </div>
        </div>
    </div>
  )
}

export default ButtonsNavigation;