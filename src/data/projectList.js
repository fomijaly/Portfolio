import logoOnepoint from "../assets/logo-onepoint.png";
import logoAso from "../assets/contenu/logo_app_aso.png";
import logoTimeCook from "../assets/contenu/logo_timecook.png";
import miniatureAso from "../assets/contenu/aso_mockupv2.png";
import infographyAso from "../assets/contenu/infography_aso_big.png";
import alternanceRecherchee from "../assets/contenu/Alternance_recherchée.gif";
import alternance from "../assets/contenu/Alternance.png";
import bootstrap from "../assets/contenu/bootstrap.png";
import codeLogo from "../assets/contenu/logo-code.png";
import cssLogo from "../assets/contenu/css.png";
import fizzbuzz from "../assets/contenu/fizzbuzz.png";
import guessTheNumber from "../assets/contenu/guessthenumber.png";
import htmlLogo from "../assets/contenu/html.png";
import imageCarteDeVisite from "../assets/contenu/image_cartedevisite.png";
import imageMinijeuPresentation from "../assets/contenu/image_minijeu_presentation.png";
import imagePortfolio from "../assets/contenu/image_portfolio.jpeg";
import imagePortfolioPerso from "../assets/contenu/image_portfolioperso.png";
import imageTodoList from "../assets/contenu/image_todolist.png";
import infographyTimecook from "../assets/contenu/infography_timecook_big.png";
import logoOpenclassroom from "../assets/contenu/logo_openclassroom.jpg";
import logoMuseumLavender from "../assets/contenu/logo-museum-lavender.png";
import magicEightBall from "../assets/contenu/magicEightBall-2.png";
import marineThomasImg from "../assets/contenu/Marine_Thomas_img.png";
import menuClose from "../assets/contenu/menu-close.png";
import menuIconWhite from "../assets/contenu/menu-icon-white.png";
import infographyMuseeLavande from "../assets/contenu/infographyMuseeLavande.png";
import partagezSiteRecette from "../assets/contenu/Partagez_site_recette.png";
import photoDeProfil from "../assets/contenu/photo_de_profil.png";
import projetMuseeLavande from "../assets/contenu/projet_museeLavande.png";
import simplonLogo from "../assets/contenu/simplon-logo.png";
import tictactoe from "../assets/contenu/tictactoe.png";
import timecookPreview from "../assets/contenu/timecook-removebg-preview.png";
import imageCompteur from "../assets/contenu/imageCompteurMairie.png";
import imageCloneMarmiton from "../assets/contenu/cloneMarmiton.png";
import imageRickEtMorty from "../assets/contenu/rickEtMorty.png";
import imageStephenKing from "../assets/contenu/stephenKing.png";

const projectsList = [
  {
    id: 1,
    title: "ASO",
    miniature: miniatureAso,
    miniDescription: "Application IOS",
    logo: logoAso,
    skills: ["Swift", "SwiftUI", "Figma"],
    category: "Formation",
    date: "Janvier 2023",
    descriptionTitle:
      "📱✊ A.SO (Alimentation Solidaire) : Lutte contre la précarité alimentaire ",
    description:
      "La précarité alimentaire vous préoccupe? Participer à cette lutte contre la faim est une cause qui vous tient à coeur?\n\n A.SO est une application très simple qui vous permettra d'agir à votre échelle. \n ✅ Les associations et organismes proposent des missions ponctuelles ou permanentes auxquelles vous pouvez répondre selon votre disponibilité et votre localisation.\n ✅ Vous pouvez également participer en réalisant des dons financiers.",
    descriptionImage: infographyAso,
    url: "#",
    github: "https://github.com/MarineThms/ASO_PrototypeApp_V1",
  },
  {
    id: 2,
    title: "Time Cook",
    miniature: timecookPreview,
    miniDescription: "Application IOS",
    logo: logoTimeCook,
    skills: ["Swift", "SwiftUI", "Figma"],
    category: "Formation",
    date: "Mars 2023",
    descriptionTitle: "📱🧑‍🍳 Time Cook : Cuisinez rapide, simple. ",
    description:
      "Vous n avez pas le temps de faire à manger?\n Pas le temps de scroller infiniment sur les pages web pour trouver LA recette rapide ET à votre goût? \n Time Cook est une application de cuisine qui s'adapte à vous.\n ✅ Des recettes vous sont proposées selon votre temps disponible.\n ✅ Elles sont également proposées selon les produits présents chez vous. \n ✅Selon vos goûts, les recettes peuvent être filtrées.",
    descriptionImage: infographyTimecook,
    url: "#",
    github: "https://github.com/MarineThms/TimeToCOok",
  },
  {
    id: 3,
    title: "Portfolio V1",
    miniature: imagePortfolio,
    miniDescription: "Version 1 de mon Portfolio en HTML, CSS et JS",
    logo: photoDeProfil,
    skills: ["Swift", "SwiftUI", "Figma"],
    category: "Perso",
    date: "Mars 2023",
    descriptionTitle: "📱🧑‍🍳 Time Cook : Cuisinez rapide, simple. ",
    description:
      "Vous n avez pas le temps de faire à manger?\n Pas le temps de scroller infiniment sur les pages web pour trouver LA recette rapide ET à votre goût? \n Time Cook est une application de cuisine qui s'adapte à vous.\n ✅ Des recettes vous sont proposées selon votre temps disponible.\n ✅ Elles sont également proposées selon les produits présents chez vous. \n ✅Selon vos goûts, les recettes peuvent être filtrées.",
    descriptionImage: "#",
    url: "https://portfolio.marinethomas.ovh/portfolio.html",
    github: "#",
  },
  {
    id: 4,
    title: "Gestion de recette",
    miniature: imagePortfolioPerso,
    miniDescription: "Version 1 du projet de recette en PHP",
    logo: photoDeProfil,
    skills: ["Swift", "SwiftUI", "Figma"],
    category: "Perso",
    date: "Mars 2023",
    descriptionTitle: "📱🧑‍🍳 Time Cook : Cuisinez rapide, simple. ",
    description:
      "Vous n avez pas le temps de faire à manger?\n Pas le temps de scroller infiniment sur les pages web pour trouver LA recette rapide ET à votre goût? \n Time Cook est une application de cuisine qui s'adapte à vous.\n ✅ Des recettes vous sont proposées selon votre temps disponible.\n ✅ Elles sont également proposées selon les produits présents chez vous. \n ✅Selon vos goûts, les recettes peuvent être filtrées.",
    descriptionImage: "#",
    url: "https://marinethomas.ovh/portfolio.html",
    github: "#",
  },
  {
    id: 5,
    title: "Carte de visite",
    miniature: imageCarteDeVisite,
    miniDescription: "Demande pour intégrer l'OF Simplon",
    logo: photoDeProfil,
    skills: ["Swift", "SwiftUI", "Figma"],
    category: "Perso",
    date: "Mars 2023",
    descriptionTitle: "📱🧑‍🍳 Time Cook : Cuisinez rapide, simple. ",
    description:
      "Vous n avez pas le temps de faire à manger?\n Pas le temps de scroller infiniment sur les pages web pour trouver LA recette rapide ET à votre goût? \n Time Cook est une application de cuisine qui s'adapte à vous.\n ✅ Des recettes vous sont proposées selon votre temps disponible.\n ✅ Elles sont également proposées selon les produits présents chez vous. \n ✅Selon vos goûts, les recettes peuvent être filtrées.",
    descriptionImage: "#",
    url: "http://cartedevisite.marinethomas.ovh",
    github: "#",
  },
  {
    id: 6,
    title: "To-do List",
    miniature: imageTodoList,
    miniDescription: "Mini projet JS",
    logo: photoDeProfil,
    skills: ["Swift", "SwiftUI", "Figma"],
    category: "Perso",
    date: "Mars 2023",
    descriptionTitle: "📱🧑‍🍳 Time Cook : Cuisinez rapide, simple. ",
    description:
      "Vous n avez pas le temps de faire à manger?\n Pas le temps de scroller infiniment sur les pages web pour trouver LA recette rapide ET à votre goût? \n Time Cook est une application de cuisine qui s'adapte à vous.\n ✅ Des recettes vous sont proposées selon votre temps disponible.\n ✅ Elles sont également proposées selon les produits présents chez vous. \n ✅Selon vos goûts, les recettes peuvent être filtrées.",
    descriptionImage: "#",
    url: "#",
    github: "#",
  },
  {
    id: 7,
    title: "Mini-jeu",
    miniature: imageMinijeuPresentation,
    miniDescription: "Mini projet JS",
    logo: photoDeProfil,
    skills: ["Swift", "SwiftUI", "Figma"],
    category: "Perso",
    date: "Mars 2023",
    descriptionTitle: "📱🧑‍🍳 Time Cook : Cuisinez rapide, simple. ",
    description:
      "Vous n avez pas le temps de faire à manger?\n Pas le temps de scroller infiniment sur les pages web pour trouver LA recette rapide ET à votre goût? \n Time Cook est une application de cuisine qui s'adapte à vous.\n ✅ Des recettes vous sont proposées selon votre temps disponible.\n ✅ Elles sont également proposées selon les produits présents chez vous. \n ✅Selon vos goûts, les recettes peuvent être filtrées.",
    descriptionImage: "#",
    url: "#",
    github: "#",
  },
  {
    id: 8,
    title: "FizzBuzz",
    miniature: fizzbuzz,
    miniDescription: "Mini projet JS",
    logo: photoDeProfil,
    skills: ["Swift", "SwiftUI", "Figma"],
    category: "Perso",
    date: "Mars 2023",
    descriptionTitle: "📱🧑‍🍳 Time Cook : Cuisinez rapide, simple. ",
    description:
      "Vous n avez pas le temps de faire à manger?\n Pas le temps de scroller infiniment sur les pages web pour trouver LA recette rapide ET à votre goût? \n Time Cook est une application de cuisine qui s'adapte à vous.\n ✅ Des recettes vous sont proposées selon votre temps disponible.\n ✅ Elles sont également proposées selon les produits présents chez vous. \n ✅Selon vos goûts, les recettes peuvent être filtrées.",
    descriptionImage: "",
    url: "#",
    github: "#",
  },
  {
    id: 9,
    title: "Guess the number",
    miniature: guessTheNumber,
    miniDescription: "Mini projet JS",
    logo: photoDeProfil,
    skills: ["Swift", "SwiftUI", "Figma"],
    category: "Perso",
    date: "Mars 2023",
    descriptionTitle: "📱🧑‍🍳 Time Cook : Cuisinez rapide, simple. ",
    description:
      "Vous n avez pas le temps de faire à manger?\n Pas le temps de scroller infiniment sur les pages web pour trouver LA recette rapide ET à votre goût? \n Time Cook est une application de cuisine qui s'adapte à vous.\n ✅ Des recettes vous sont proposées selon votre temps disponible.\n ✅ Elles sont également proposées selon les produits présents chez vous. \n ✅Selon vos goûts, les recettes peuvent être filtrées.",
    descriptionImage: "#",
    url: "#",
    github: "#",
  },
  {
    id: 10,
    title: "Tic Tac Toe",
    miniature: tictactoe,
    miniDescription: "Mini projet JS",
    logo: photoDeProfil,
    skills: ["Swift", "SwiftUI", "Figma"],
    category: "Perso",
    date: "Mars 2023",
    descriptionTitle: "📱🧑‍🍳 Time Cook : Cuisinez rapide, simple. ",
    description:
      "Vous n avez pas le temps de faire à manger?\n Pas le temps de scroller infiniment sur les pages web pour trouver LA recette rapide ET à votre goût? \n Time Cook est une application de cuisine qui s'adapte à vous.\n ✅ Des recettes vous sont proposées selon votre temps disponible.\n ✅ Elles sont également proposées selon les produits présents chez vous. \n ✅Selon vos goûts, les recettes peuvent être filtrées.",
    descriptionImage: "#",
    url: "#",
    github: "#",
  },
  {
    id: 11,
    title: "Magic Eight Ball",
    miniature: magicEightBall,
    miniDescription: "Mini projet JS",
    logo: photoDeProfil,
    skills: ["Swift", "SwiftUI", "Figma"],
    category: "Perso",
    date: "Mars 2023",
    descriptionTitle: "📱🧑‍🍳 Time Cook : Cuisinez rapide, simple. ",
    description:
      "Vous n avez pas le temps de faire à manger?\n Pas le temps de scroller infiniment sur les pages web pour trouver LA recette rapide ET à votre goût? \n Time Cook est une application de cuisine qui s'adapte à vous.\n ✅ Des recettes vous sont proposées selon votre temps disponible.\n ✅ Elles sont également proposées selon les produits présents chez vous. \n ✅Selon vos goûts, les recettes peuvent être filtrées.",
    descriptionImage: "#",
    url: "#",
    github: "#",
  },
  {
    id: 12,
    title: "Musée de lavande",
    miniature: projetMuseeLavande,
    miniDescription: "Mini projet JS",
    logo: projetMuseeLavande,
    skills: ["Swift", "SwiftUI", "Figma"],
    category: "Formation",
    date: "Mars 2023",
    descriptionTitle: "📱🧑‍🍳 Time Cook : Cuisinez rapide, simple. ",
    description:
      "Vous n avez pas le temps de faire à manger?\n Pas le temps de scroller infiniment sur les pages web pour trouver LA recette rapide ET à votre goût? \n Time Cook est une application de cuisine qui s'adapte à vous.\n ✅ Des recettes vous sont proposées selon votre temps disponible.\n ✅ Elles sont également proposées selon les produits présents chez vous. \n ✅Selon vos goûts, les recettes peuvent être filtrées.",
    descriptionImage: infographyMuseeLavande,
    url: "https://project-lavender-museum-marine-thomas-88fc35d3517a11fba2bfad3bd.gitlab.io",
    github: "#",
  },
  {
    id: 13,
    title: "Mairie Toulouse",
    miniature: imageCompteur,
    miniDescription: "Mini projet JS",
    logo: photoDeProfil,
    skills: ["Swift", "SwiftUI", "Figma"],
    category: "Formation",
    date: "Mars 2023",
    descriptionTitle: "📱🧑‍🍳 Time Cook : Cuisinez rapide, simple. ",
    description:
      "Vous n avez pas le temps de faire à manger?\n Pas le temps de scroller infiniment sur les pages web pour trouver LA recette rapide ET à votre goût? \n Time Cook est une application de cuisine qui s'adapte à vous.\n ✅ Des recettes vous sont proposées selon votre temps disponible.\n ✅ Elles sont également proposées selon les produits présents chez vous. \n ✅Selon vos goûts, les recettes peuvent être filtrées.",
    descriptionImage: "#",
    url: "https://compteur-mairie-toulouse-fomijaly-fb9026ef16b24e2e0f4abb9e7353d.gitlab.io",
    github: "#",
  },
  {
    id: 14,
    title: "Site Marmiton",
    miniature: imageCloneMarmiton,
    miniDescription: "Mini projet JS",
    logo: photoDeProfil,
    skills: ["Swift", "SwiftUI", "Figma"],
    category: "Perso",
    date: "Mars 2023",
    descriptionTitle: "📱🧑‍🍳 Time Cook : Cuisinez rapide, simple. ",
    description:
      "Vous n avez pas le temps de faire à manger?\n Pas le temps de scroller infiniment sur les pages web pour trouver LA recette rapide ET à votre goût? \n Time Cook est une application de cuisine qui s'adapte à vous.\n ✅ Des recettes vous sont proposées selon votre temps disponible.\n ✅ Elles sont également proposées selon les produits présents chez vous. \n ✅Selon vos goûts, les recettes peuvent être filtrées.",
    descriptionImage: "#",
    url: "https://marmiton-clone-fomijaly-95db68a0e3a8e07bf3aee0f2f0368b44b7f011b.gitlab.io",
    github: "#",
  },
  {
    id: 15,
    title: "Rick & Morty",
    miniature: imageRickEtMorty,
    miniDescription: "Mini projet JS",
    logo: photoDeProfil,
    skills: ["Swift", "SwiftUI", "Figma"],
    category: "Perso",
    date: "Mars 2023",
    descriptionTitle: "📱🧑‍🍳 Time Cook : Cuisinez rapide, simple. ",
    description:
      "Vous n avez pas le temps de faire à manger?\n Pas le temps de scroller infiniment sur les pages web pour trouver LA recette rapide ET à votre goût? \n Time Cook est une application de cuisine qui s'adapte à vous.\n ✅ Des recettes vous sont proposées selon votre temps disponible.\n ✅ Elles sont également proposées selon les produits présents chez vous. \n ✅Selon vos goûts, les recettes peuvent être filtrées.",
    descriptionImage: "#",
    url: "https://rick-and-morty-fomijaly-d2e54c8eb4cbae88699c762d4971a611ce630aa.gitlab.io/",
    github: "#",
  },
  {
    id: 16,
    title: "Stephen King",
    miniature: imageStephenKing,
    miniDescription: "Mini projet JS",
    logo: photoDeProfil,
    skills: ["Swift", "SwiftUI", "Figma"],
    category: "Formation",
    date: "Mars 2023",
    descriptionTitle: "📱🧑‍🍳 Time Cook : Cuisinez rapide, simple. ",
    description:
      "Vous n avez pas le temps de faire à manger?\n Pas le temps de scroller infiniment sur les pages web pour trouver LA recette rapide ET à votre goût? \n Time Cook est une application de cuisine qui s'adapte à vous.\n ✅ Des recettes vous sont proposées selon votre temps disponible.\n ✅ Elles sont également proposées selon les produits présents chez vous. \n ✅Selon vos goûts, les recettes peuvent être filtrées.",
    descriptionImage: "#",
    url: "https://fandom-stephen-king-mt.netlify.app",
    github: "#",
  },
  {
    id: 17,
    title: "Signal",
    miniature: "",
    miniDescription: "Mini projet JS",
    logo: photoDeProfil,
    skills: ["Swift", "SwiftUI", "Figma"],
    category: "Formation",
    date: "Mars 2023",
    descriptionTitle: "📱🧑‍🍳 Time Cook : Cuisinez rapide, simple. ",
    description:
      "Vous n avez pas le temps de faire à manger?\n Pas le temps de scroller infiniment sur les pages web pour trouver LA recette rapide ET à votre goût? \n Time Cook est une application de cuisine qui s'adapte à vous.\n ✅ Des recettes vous sont proposées selon votre temps disponible.\n ✅ Elles sont également proposées selon les produits présents chez vous. \n ✅Selon vos goûts, les recettes peuvent être filtrées.",
    descriptionImage: "#",
    url: "https://signal-mt.netlify.app",
    github: "#",
  },
  {
    id: 18,
    title: "Région Toulouse",
    miniature: "",
    miniDescription: "Mini projet JS",
    logo: photoDeProfil,
    skills: ["Swift", "SwiftUI", "Figma"],
    category: "Formation",
    date: "Mars 2023",
    descriptionTitle: "📱🧑‍🍳 Time Cook : Cuisinez rapide, simple. ",
    description:
      "Vous n avez pas le temps de faire à manger?\n Pas le temps de scroller infiniment sur les pages web pour trouver LA recette rapide ET à votre goût? \n Time Cook est une application de cuisine qui s'adapte à vous.\n ✅ Des recettes vous sont proposées selon votre temps disponible.\n ✅ Elles sont également proposées selon les produits présents chez vous. \n ✅Selon vos goûts, les recettes peuvent être filtrées.",
    descriptionImage: "#",
    url: "https://toulouse-activites.netlify.app",
    github: "#",
  },
  {
    id: 19,
    title: "Netflim",
    miniature: "",
    miniDescription: "Mini projet JS",
    logo: photoDeProfil,
    skills: ["Swift", "SwiftUI", "Figma"],
    category: "Formation",
    date: "Mars 2023",
    descriptionTitle: "📱🧑‍🍳 Time Cook : Cuisinez rapide, simple. ",
    description:
      "Vous n avez pas le temps de faire à manger?\n Pas le temps de scroller infiniment sur les pages web pour trouver LA recette rapide ET à votre goût? \n Time Cook est une application de cuisine qui s'adapte à vous.\n ✅ Des recettes vous sont proposées selon votre temps disponible.\n ✅ Elles sont également proposées selon les produits présents chez vous. \n ✅Selon vos goûts, les recettes peuvent être filtrées.",
    descriptionImage: "#",
    url: "https://netflim-fomijaly-de27ca527cb715974375d51a2733b35031454e8d2b34fb.gitlab.io",
    github: "#",
  },
  {
    id: 20,
    title: "Fonditas",
    miniature: "",
    miniDescription: "Mini projet JS",
    logo: photoDeProfil,
    skills: ["Swift", "SwiftUI", "Figma"],
    category: "Formation",
    date: "Mars 2023",
    descriptionTitle: "📱🧑‍🍳 Time Cook : Cuisinez rapide, simple. ",
    description:
      "Vous n avez pas le temps de faire à manger?\n Pas le temps de scroller infiniment sur les pages web pour trouver LA recette rapide ET à votre goût? \n Time Cook est une application de cuisine qui s'adapte à vous.\n ✅ Des recettes vous sont proposées selon votre temps disponible.\n ✅ Elles sont également proposées selon les produits présents chez vous. \n ✅Selon vos goûts, les recettes peuvent être filtrées.",
    descriptionImage: "#",
    url: "https://fonditas2-fomijaly-f946fc2c4001635488a237c9f6922ce773a7f79eac26.gitlab.io",
    github: "#",
  },
  {
    id: 21,
    title: "Harry Potter",
    miniature: "",
    miniDescription: "Mini projet JS",
    logo: photoDeProfil,
    skills: ["Swift", "SwiftUI", "Figma"],
    category: "Formation",
    date: "Mars 2023",
    descriptionTitle: "📱🧑‍🍳 Time Cook : Cuisinez rapide, simple. ",
    description:
      "Vous n avez pas le temps de faire à manger?\n Pas le temps de scroller infiniment sur les pages web pour trouver LA recette rapide ET à votre goût? \n Time Cook est une application de cuisine qui s'adapte à vous.\n ✅ Des recettes vous sont proposées selon votre temps disponible.\n ✅ Elles sont également proposées selon les produits présents chez vous. \n ✅Selon vos goûts, les recettes peuvent être filtrées.",
    descriptionImage: "#",
    url: "https://fonditas2-fomijaly-f946fc2c4001635488a237c9f6922ce773a7f79eac26.gitlab.io",
    github: "#",
  },
];

export default projectsList;
