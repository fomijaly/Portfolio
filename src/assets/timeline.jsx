let timelineElements = [
    {
        "id":1,
        "title": "Webmaster",
        "tag": "Professionnel",
        "location": "ATTER, Albi 81 000",
        "description": "Maintenance d'un intranet Wordpress et découverte de la programmation en PHP.",
        "first_date": "Avril 2019",
        "last_date": "Août 2022"
    },
    {
        "id":2,
        "title": "Auto didacte",
        "tag": "Personnel",
        "location": null,
        "description": "Formation en autodidacte afin d'apprendre les bases de la programmation",
        "first_date": "Avril 2019",
        "last_date": null
    },
    {
        "id":3,
        "title": "Apple Foundation Program",
        "tag": "Formation",
        "location": "Simplon, 31 000 Toulouse",
        "description": "Conception & développement du prototype d'application A.S.O",
        "first_date": "Janvier 2023",
        "last_date": null
    },
    {
        "id":4,
        "title": "Apple Foundation Program Extended",
        "tag": "Formation",
        "location": "Simplon, 31 000 Toulouse",
        "description": "Conception & développement du prototype d'application Time Cook",
        "first_date": "Mars 2023",
        "last_date": null
    },
    {
        "id":5,
        "title": "Parcours Hackeuses",
        "tag": "Formation",
        "location": "Simplon, 31 000 Toulouse",
        "description": "Passage de la certification Wordpress",
        "first_date": "Septembre 2023",
        "last_date": null
    },
    {
        "id":6,
        "title": "Parcours CDA intensif",
        "tag": "Formation",
        "location": "Simplon, 31 000 Toulouse",
        "description": "Préparation à une entrée entreprise",
        "first_date": "Octobre 2023",
        "last_date": "Janvier 2023"
    },
    {
        "id":7,
        "title": "Formation CDA en alternance",
        "tag": "Formation",
        "location": "Simplon, 31 000 Toulouse",
        "description": "Mise en pratique des notions en milieu professionnel",
        "first_date": "29 Janvier 2024",
        "last_date": "Aujourd'hui"
    },
    {
        "id":8,
        "title": "OnePoint",
        "tag": "Professionnel",
        "location": "31 000 Toulouse",
        "description": "Mise en pratique des notions en milieu professionnel",
        "first_date": "29 Janvier 2024",
        "last_date": "Aujourd'hui"
    }
]

export default timelineElements;