import Figma from "../assets/logoSkills/icon-figma.png";
import Canva from "../assets/logoSkills/icon-canva.png";
import Html from "../assets/logoSkills/icon-html.png";
import Css from "../assets/logoSkills/icon-css.png";
import JS from "../assets/logoSkills/icon-javascript.png";
import Bootstrap from "../assets/logoSkills/icon-bootstrap.png";
import React from "../assets/logoSkills/icon-react.png";
import Ios from "../assets/logoSkills/icon-ios.png";
import Java from "../assets/logoSkills/icon-java.png";
import SpringBoot from "../assets/logoSkills/icon-springboot.png";
import Php from "../assets/logoSkills/icon-php.png";
import Sql from "../assets/logoSkills/icon-sql.png";
import Phpmyadmin from "../assets/logoSkills/icon-phpmyadmin.png";
import Github from "../assets/logoSkills/icon-github.png";
import Filezilla from "../assets/logoSkills/icon-filezilla.png";
import Wordpress from "../assets/logoSkills/icon-wordpress.png";
import Angular from "../assets/logoSkills/icon-angular.png";
import Git from "../assets/logoSkills/icon-git.png";
import Gitlab from "../assets/logoSkills/icon-gitlab.png";
import Docker from "../assets/logoSkills/icon-docker.png";

let skills = [
  {
    id: 1,
    title: "Figma",
    class: "btn-hard-skill rounded-pill px-3 fw-semibold",
    tag: "Design",
    picture: Figma,
  },
  {
    id: 2,
    title: "Canva",
    class: "btn-hard-skill rounded-pill px-3 fw-semibold",
    tag: "Design",
    picture: Canva,
  },
  {
    id: 3,
    title: "HTML5",
    class: "btn-hard-skill rounded-pill px-3 fw-semibold",
    tag: "Front",
    picture: Html,
  },
  {
    id: 4,
    title: "CSS3",
    class: "btn-hard-skill rounded-pill px-3 fw-semibold",
    tag: "Front",
    picture: Css,
  },
  {
    id: 5,
    title: "Javascript",
    class: "btn-hard-skill rounded-pill px-3 fw-semibold",
    tag: "Front",
    picture: JS,
  },
  {
    id: 6,
    title: "Bootstrap",
    class: "btn-hard-skill rounded-pill px-3 fw-semibold",
    tag: "Front",
    picture: Bootstrap,
  },
  {
    id: 7,
    title: "React",
    class: "btn-hard-skill rounded-pill px-3 fw-semibold",
    tag: "Front",
    picture: React,
  },
  {
    id: 8,
    title: "Angular",
    class: "btn-hard-skill rounded-pill px-3 fw-semibold",
    tag: "Front",
    picture: Angular,
  },
  {
    id: 9,
    title: "SwiftUI",
    class: "btn-hard-skill rounded-pill px-3 fw-semibold",
    tag: "Front",
    picture: Ios,
  },
  {
    id: 10,
    title: "Java",
    class: "btn-hard-skill rounded-pill px-3 fw-semibold",
    tag: "Back",
    picture: Java,
  },
  {
    id: 11,
    title: "Spring Boot",
    class: "btn-hard-skill rounded-pill px-3 fw-semibold",
    tag: "Back",
    picture: SpringBoot,
  },
  {
    id: 12,
    title: "PHP7",
    class: "btn-hard-skill rounded-pill px-3 fw-semibold",
    tag: "Back",
    picture: Php,
  },
  {
    id: 13,
    title: "SQL",
    class: "btn-hard-skill rounded-pill px-3 fw-semibold",
    tag: "Back",
    picture: Sql,
  },
  {
    id: 14,
    title: "PhpMyAdmin",
    class: "btn-hard-skill rounded-pill px-3 fw-semibold",
    tag: "Back",
    picture: Phpmyadmin,
  },
  {
    id: 15,
    title: "Git",
    class: "btn-hard-skill rounded-pill px-3 fw-semibold",
    tag: "Other",
    picture: Git,
  },
  {
    id: 16,
    title: "Github",
    class: "btn-hard-skill rounded-pill px-3 fw-semibold",
    tag: "Other",
    picture: Github,
  },
  {
    id: 17,
    title: "Gitlab",
    class: "btn-hard-skill rounded-pill px-3 fw-semibold",
    tag: "Other",
    picture: Gitlab,
  },
  {
    id: 18,
    title: "Filezilla",
    class: "btn-hard-skill rounded-pill px-3 fw-semibold",
    tag: "Other",
    picture: Filezilla,
  },
  {
    id: 19,
    title: "Wordpress",
    class: "btn-hard-skill rounded-pill px-3 fw-semibold",
    tag: "Other",
    picture: Wordpress,
  },
  {
    id: 20,
    title: "Docker",
    class: "btn-hard-skill rounded-pill px-3 fw-semibold",
    tag: "Other",
    picture: Docker,
  },
  {
    id: 21,
    title: "Autonomie",
    class: "btn-soft-skill rounded-pill px-3 fw-semibold",
    tag: "Soft",
    picture: "✨",
  },
  {
    id: 22,
    title: "Créativité",
    class: "btn-soft-skill rounded-pill px-3 fw-semibold",
    tag: "Soft",
    picture: "🌈",
  },
  {
    id: 23,
    title: "Travail d'équipe",
    class: "btn-soft-skill rounded-pill px-3 fw-semibold",
    tag: "Soft",
    picture: "🤝",
  },
  {
    id: 24,
    title: "Motivation",
    class: "btn-soft-skill rounded-pill px-3 fw-semibold",
    tag: "Soft",
    picture: "💪🏽",
  },
  {
    id: 25,
    title: "Rigueur",
    class: "btn-soft-skill rounded-pill px-3 fw-semibold",
    tag: "Soft",
    picture: "👌🏽",
  },
  {
    id: 26,
    title: "Persévérance",
    class: "btn-soft-skill rounded-pill px-3 fw-semibold",
    tag: "Soft",
    picture: "⚡️",
  },
];

export default skills;
