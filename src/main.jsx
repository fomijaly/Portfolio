import React from "react";
import ReactDOM from "react-dom/client";
import Home from "./pages/Home/Home";
import Navigation from "./components/Navigation/Navigation";
import DetailProject from "./pages/DetailProject/DetailProject";
import "./index.css";
import { RouterProvider, ScrollRestoration, createBrowserRouter } from "react-router-dom";
import Presentation from "./pages/Presentation/Presentation";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Home />,
  },
  {
    path: "/",
    element: <Navigation />,
  },
  {
    path: "/detail/:id",
    element: <DetailProject />,
  },
  {
    path: "/timeline",
    element: <Presentation/>,
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
);
