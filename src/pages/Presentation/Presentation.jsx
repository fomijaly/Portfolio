import React from "react";
import "./Presentation.css";
import Timeline from "../../components/Timeline/Timeline";
import { useNavigate } from "react-router-dom";
import { ScrollRestoration } from 'react-router-dom';

function Presentation() {
  let navigate = useNavigate();

  return (
    <>
    <ScrollRestoration/>
    <div className="timeline_header d-flex flex-column justify-content-between">
      <span className="btn-back d-flex align-items-center fw-medium mb-5" onClick={() => navigate(-1)}>
        <i className="bx bx-chevron-left bx-sm"></i>Retour
      </span>
      <h1 className="timeline_h1">⏳ Mon parcours</h1>
      <p>Je poursuis aujourd'hui mon parcours en tant que développeuse. 
        <br /> Forte des expériences citées ci-dessous, je me sens plus à l'aise avec ce métier.
        <br /> Je continue à me former au sein de mon entreprise actuelle.</p>
    </div>
    <section id="timeline" className="mainTimeline">
      <div className="timeline d-flex flex-column align-items-center justify-content-center">
        <Timeline />
      </div>
    </section>
    </>
  );
}

export default Presentation;
