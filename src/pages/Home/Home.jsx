import React from 'react';
import './Home.css';
import Navigation from '../../components/Navigation/Navigation';
import Competence from '../../components/Competence/Competence';
import MainInformation from '../../components/MainInformation/MainInformation';
import Project from '../../components/Project/Project';
import ButtonTop from '../../components/Buttons/ButtonTop';
import AboutMe from "../../components/Presentation/AboutMe";

function Home(){
  return(
    <>
      <ButtonTop/>
      <MainInformation/>
      <Navigation/>
      <AboutMe/>
      <Competence/>
      <Project/>
    </>
  )
}

export default Home;
