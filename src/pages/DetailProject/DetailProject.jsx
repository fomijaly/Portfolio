import React, { useEffect, useState } from "react";
import "./DetailProject.css";
import { useNavigate, useParams } from "react-router-dom";
import data from "../../data/projectList";

function DetailProject() {
  const { id } = useParams();
  let navigate = useNavigate();
  const [currentProject, setCurrentProject] = useState([]);

  useEffect(() => {
    const foundValue = data.find((project) => project.id === parseInt(id, 10));
    setCurrentProject(foundValue);
  }, []);

  return (
    <section>
      <span
        className="btn-back d-flex align-items-center fw-medium"
        onClick={() => navigate(-1)}
      >
        <i className="bx bx-chevron-left bx-sm"></i>Retour
      </span>
      <div className="d-flex justify-content-between align-items-center my-4">
        <h1>{currentProject.title}</h1>
        <img src={currentProject.logo} alt={currentProject.title} />
      </div>
      <div className="d-flex flex-column mb-4">
        <div className="d-flex align-items-center gap-2">
          <i className="bx bxs-user"></i>
          <b>Compétences :{currentProject.skills}</b>
        </div>
        <hr />
        <div className="d-flex flex-row justify-content-between">
          <span>{currentProject.category}</span>
          <span>{currentProject.date}</span>
        </div>
      </div>
      <p>{currentProject.descriptionTitle}</p>
      <p className="project_description">{currentProject.description}</p>
      <img src={currentProject.descriptionImage} alt="" />
    </section>
  );
}

export default DetailProject;
